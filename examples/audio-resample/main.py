import sys
from media_processing_lib.audio import audio_read, audio_write

def main():
    new_sample_rate = float(sys.argv[3])
    audio = audio_read(sys.argv[1])
    print(f"Read '{sys.argv[1]}'. {audio}")
    new_audio = audio.resample(new_sample_rate)
    print(f"Resampled. {new_audio}")
    audio_write(new_audio, sys.argv[2])
    print(f"Saved to {sys.argv[2]}")

if __name__ == "__main__":
    main()
