from media_processing_lib.collage_maker import collage_fn
from media_processing_lib.image import image_read, image_resize, image_add_title, image_write
from pathlib import Path
import matplotlib.pyplot as plt
import sys

def main():
    images_paths = (Path(__file__).absolute().parent/"images/").glob("*.jpg")
    images = [image_read(x) for x in images_paths]
    if len(sys.argv) == 2:
        height, width = map(int, sys.argv[1].split(","))
    else:
        height, width = 240, 426
    resized_images = [image_resize(x, height=height, width=width) for x in images]
    combined_image = collage_fn(resized_images, (2, 2), titles=[f"Image {i}" for i in range(len(images))])
    titled_image = image_add_title(combined_image, "General Title", font_color="red", background_color="blue")
    image_write(titled_image, f"result_{height}x{width}.png")
    plt.imshow(titled_image)
    plt.show()

if __name__ == "__main__":
    main()
