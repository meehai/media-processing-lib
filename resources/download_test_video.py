# pylint: disable=all
import numpy as np
from media_processing_lib.video.backends.imageio import MPLImageIOBackend
from media_processing_lib.image import image_write
from pathlib import Path
import os
import gdown

video_url = "https://drive.google.com/uc?id=1Wc1LrYIuyovpdFwPKY5NyKH1JZmJY5gj"
video_path = Path(os.getenv("VIDEO_PATH", Path(__file__).parent / "test_video.mp4"))
video_path.parent.mkdir(exist_ok=True)
if not video_path.exists():
    gdown.download(video_url, video_path.__str__())

v = MPLImageIOBackend(video_path)
Path(f"{video_path.parent}/images_path").mkdir(exist_ok=True, parents=True)
Path(f"{video_path.parent}/numpy_path").mkdir(exist_ok=True, parents=True)
for i in range(100):
    image_write(v[130 + i], f"{video_path.parent}/images_path/{i}.png")
    np.savez(f"{video_path.parent}/numpy_path/{i}.npz", v[130 + i])
