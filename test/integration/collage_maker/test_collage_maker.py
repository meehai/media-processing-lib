import tempfile
import shutil
import numpy as np
from pathlib import Path
from media_processing_lib.image import image_write
from media_processing_lib.collage_maker import CollageMaker

def test_collage_maker_1():
    images = np.random.randint(0, 255, size=(3, 10, 30, 30, 3), dtype=np.uint8)
    tmp_dir = Path(tempfile.mkdtemp())
    for i in range(images.shape[0]):
        (tmp_dir / f"{i}").mkdir(exist_ok=True, parents=True)
        for j in range(images.shape[1]):
            image_write(images[i][j], f"{tmp_dir}/{i}/{j}.png")

    files = [[f"{tmp_dir}/{i}/{j}.png" for j in range(images.shape[1])] for i in range(images.shape[0])]
    plot_fns = lambda x: x
    output_dir = tmp_dir / "output_dir"
    maker = CollageMaker(files, plot_fns, output_dir)
    maker.make_collage()
    shutil.rmtree(tmp_dir)
