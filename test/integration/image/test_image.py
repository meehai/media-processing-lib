import numpy as np
from media_processing_lib.image import image_read, image_write, image_resize
from media_processing_lib.image.libs_builder import get_available_image_libs

def test_read_write_1():
    image = np.random.randint(0, 255, size=(30, 30, 3))
    for img_lib in get_available_image_libs():
        image_write(image, f"/tmp/image_{img_lib}.png", img_lib=img_lib)

    for img_lib in get_available_image_libs():
        _image_read = image_read(f"/tmp/image_{img_lib}.png")
        assert np.abs(_image_read - image).sum() < 1e-5

def test_image_read_write_resize_1():
    image1 = np.ones(shape=(30, 30, 3), dtype=np.uint8)
    all_img_libs = get_available_image_libs().intersection(["PIL", "opencv"])
    all_resize_libs = get_available_image_libs().intersection(["skimage", "opencv", "PIL"])
    all_interpolations = ["nearest", "bilinear", "bicubic"]

    for img_lib1 in all_img_libs:
        for img_lib2 in all_img_libs:
            for resize_lib1 in all_resize_libs:
                for resize_lib2 in all_resize_libs:
                    image_write(image1, "/tmp/image.png", img_lib=img_lib1)
                    image2 = image_read("/tmp/image.png", img_lib=img_lib2)
                    assert np.abs(image1 - image2).sum() < 1e-5
                    for interpolation in all_interpolations:
                        img1_resized = image_resize(image1, height=15, width=15,
                                                    interpolation=interpolation, resize_lib=resize_lib1)
                        img2_resized = image_resize(image2, height=15, width=15,
                                                    interpolation=interpolation, resize_lib=resize_lib2)
                        assert (img1_resized - img2_resized).sum() < 1e-5
