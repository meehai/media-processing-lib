import numpy as np
from pathlib import Path
from typing import Dict
from media_processing_lib.video import MPLVideo, video_read
from media_processing_lib.video.utils import get_available_video_read_libs


resources_dir = Path(__file__).absolute().parents[3] / "resources"
video_path = resources_dir / "test_video.mp4"
video_libs = [x for x in get_available_video_read_libs() if x not in ["disk"]]
libs_data: Dict[str, MPLVideo] = {lib: video_read(video_path, video_lib=lib) for lib in video_libs}


def test_constructor_1():
    for video in libs_data.values():
        assert not video is None


def test_fps_1():
    fps = []
    for video in libs_data.values():
        fps.append(video.fps)
        assert video.fps == fps[0]


def test_fps_2():
    """FPS can be changed dynamically, since it only affects writing to disk."""
    for video in libs_data.values():
        assert video.fps != 99
        video.fps = 99
        assert video.fps == 99


def test_frame_shape_1():
    """If frame shape is not provided, it is infered from first frame."""
    shapes = []
    for video in libs_data.values():
        shapes.append(video.frame_shape)
        assert video.frame_shape == shapes[0]
        assert video[30].shape[0:2] == shapes[0]


def test_video_apply_1():
    shapes = []
    for video in libs_data.values():
        applied_video = video.apply(lambda item, video, t: item * 0)
        shapes.append(applied_video.frame_shape)
        assert applied_video.frame_shape == shapes[0]
        assert applied_video[30].sum() == 0


def test_video_apply_2():
    shapes = []
    for video in libs_data.values():
        applied_video = video.apply(lambda item, video, t: item * 0).apply(lambda item, vidoe, t: item + 20)
        shapes.append(applied_video.frame_shape)
        assert applied_video.frame_shape == shapes[0]
        assert applied_video[30].sum() == 20 * np.prod(applied_video.frame_shape) * 3


def main():
    # TestMPLVideo().test_apply_1()
    pass


if __name__ == "__main__":
    main()
