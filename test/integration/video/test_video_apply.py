import numpy as np
from media_processing_lib.video import video_read
from media_processing_lib.utils import get_library_root
from functools import partial

resources_dir = get_library_root() / "resources"
video_path = resources_dir / "test_video.mp4"
video = video_read(video_path, video_lib="decord")
cnt = 0


def test_video_apply_constructor_1():
    video_applied = video.apply(lambda x, v, t: np.stack([x[..., 0], x[..., 0], x[..., 0]], axis=-1))
    assert video_applied is not None


def test_video_apply_valid_1():
    """Valid operation, no shape change"""
    video_applied = video.apply(lambda x, v, t: x * 0)
    assert video_applied is not None
    frame = video_applied[5]
    assert frame.shape[0:2] == video_applied.frame_shape


def test_video_apply_invalid_1():
    """Invalid operation, but no access, so it's still good"""
    video_applied = video.apply(lambda x, v, t: x[0:5, 0:5])
    assert video_applied is not None


def test_video_apply_invalid_2():
    """Invalid operation, but no access, so it's still good"""
    video_applied = video.apply(lambda x, v, t: x[0:5, 0:5])
    assert video_applied is not None
    try:
        _ = video_applied[5]
    except:
        pass


# TODO: this test fails the CI on gitlab for some reason -- to investigate.
# def test_video_apply_lazy_stress_test_1():
#     """Valid operation, no shape change"""
#     global cnt
#     video_applied = video

#     def apply_fn(x, v, t, i):
#         global cnt
#         cnt += 1
#         y = x.copy()
#         y[0 : y.shape[0] // 2, 0 : y.shape[1] // 2] = y[0 : y.shape[0] // 2, 0 : y.shape[1] // 2] * 0 + i
#         return y

#     cnt = 0
#     for i in range(100):
#         video_applied = video_applied.apply(partial(apply_fn, i=i))
#     assert video_applied is not None

#     video_applied.write("/tmp/test.mp4")
#     assert cnt == 100 * len(video), f"{cnt} vs {100 * len(video)}"


if __name__ == "__main__":
    test_video_apply_lazy_stress_test_1()
