import numpy as np
from PIL import Image
from media_processing_lib.image.libs.pil.reader import image_read


def test_image_read_pil_grayscale_1():
    img_pil = Image.fromarray(np.random.randint(0, 255, size=(50, 50)), mode="L")
    img_pil.save("/tmp/img_gray.png")

    img = image_read("/tmp/img_gray.png")
    assert img.shape == (50, 50, 3)
    assert np.allclose(np.array(img_pil), img[..., 0])
