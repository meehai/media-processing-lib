import numpy as np
from media_processing_lib.image import image_add_border

def test_image_add_border_1():
    zeros = np.zeros((200, 200, 3), dtype=np.uint8)
    img_border = image_add_border(zeros, (255, 255, 255), thicc=1)
    assert (zeros == 0).all() # not inplace
    assert np.allclose(img_border[..., 0], img_border[..., 1]) and np.allclose(img_border[..., 0], img_border[..., 2])
    assert (img_border[1:-1, 1:-1] == 0).all() # if we remove 1st row, it's all zeros
    assert (img_border[0, 0] == 255).all()
    assert (img_border[-1, -1] == 255).all()
    assert (img_border[0, -1] == 255).all()
    assert (img_border[-1, 0] == 255).all()

def test_image_add_border_2():
    zeros = np.zeros((200, 200, 3), dtype=np.uint8)
    img_border = image_add_border(zeros, (1, 1, 1), thicc=1, add_x=True)
    assert (zeros == 0).all() # not inplace
    assert np.allclose(img_border[..., 0], img_border[..., 1]) and np.allclose(img_border[..., 0], img_border[..., 2])
    assert not (img_border[1:-1, 1:-1] == 0).all() # we added the x too (diagonal of the image)
    assert img_border[1:-1, 1:-1].sum() == 1188

def test_image_add_border_3():
    zeros = np.zeros((200, 200, 3), dtype=np.uint8)
    image_add_border(zeros, (1, 1, 1), thicc=1, add_x=True, inplace=True)
    assert not (zeros == 0).all() # not inplace
