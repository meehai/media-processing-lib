import numpy as np
from media_processing_lib.image import image_resize, image_resize_batch


def test_image_resize_1():
    image = np.random.randn(30, 30, 3)
    try:
        image = image_resize(image, height=15, width=15, interpolation="bilinear", resize_lib="skimage")
        assert False
    except Exception:
        pass

def test_image_resize_2():
    image = np.random.randint(0, 255, size=(30, 30, 3)).astype(np.uint8)

    images = {}
    for resize_lib in ["skimage", "opencv", "PIL"]:
        images[resize_lib] = image_resize(image, height=30, width=None, interpolation="nearest", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (image - images[resize_lib]).sum() < 1e-5

def test_image_resize_3():
    image = np.random.randint(0, 255, size=(30, 30, 3)).astype(np.uint8)

    images = {}
    image = image_resize(image, height=13, width=17, interpolation="bilinear", resize_lib="skimage")
    for resize_lib in ["skimage", "opencv", "PIL"]:
        images[resize_lib] = image_resize(image, height=13, width=17, interpolation="bilinear", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (image - images[resize_lib]).sum() < 1e-5

def test_image_resize_black_bars_1():
    image = np.random.randn(30, 30, 3)
    try:
        image = image_resize(image, height=15, width=15, interpolation="bilinear",
                             mode="black_bars", resize_lib="skimage")
        assert False
    except Exception:
        pass

def test_image_resize_black_bars_2():
    image = np.random.randint(0, 255, size=(30, 30, 3)).astype(np.uint8)

    images = {}
    for resize_lib in ["skimage", "opencv", "PIL"]:
        try:
            images[resize_lib] = image_resize(image, height=30, width=None, interpolation="nearest",
                                              mode="black_bars", resize_lib=resize_lib)
            assert False
        except Exception:
            pass

    for resize_lib in ["skimage", "opencv", "PIL"]:
        images[resize_lib] = image_resize(image, height=30, width=30, interpolation="nearest",
                                          mode="black_bars", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (image - images[resize_lib]).sum() < 1e-5

def test_image_resize_black_bars_3():
    image = np.random.randint(0, 255, size=(30, 30, 3)).astype(np.uint8)

    images = {}
    image = image_resize(image, height=13, width=17, interpolation="bilinear",
                         mode="black_bars", resize_lib="skimage")
    for resize_lib in ["skimage", "opencv", "PIL"]:
        images[resize_lib] = image_resize(image, height=13, width=17, interpolation="bilinear",
                                          mode="black_bars", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (image - images[resize_lib]).sum() < 1e-5

def test_image_resize_batch_1():
    N = 7
    images = np.random.randn(N, 30, 30, 3)
    try:
        images = image_resize_batch(images, height=15, width=15, interpolation="bilinear", resize_lib="skimage")
        assert False
    except Exception:
        pass

def test_image_resize_batch_2():
    N = 7
    images = np.random.randint(0, 255, size=(N, 30, 30, 3), dtype=np.uint8)

    res = {}
    for resize_lib in ["skimage", "opencv", "PIL"]:
        res[resize_lib] = image_resize_batch(images, height=30, width=None,
                                             interpolation="nearest", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (images - res[resize_lib]).sum() < 1e-5

def test_image_resize_batch_3():
    N = 7
    images = np.random.randint(0, 255, size=(N, 30, 30, 3), dtype=np.uint8)

    res = {}
    images = image_resize_batch(images, height=13, width=17, interpolation="bilinear", resize_lib="skimage")
    for resize_lib in ["skimage", "opencv", "PIL"]:
        res[resize_lib] = image_resize_batch(images, height=13, width=17,
                                             interpolation="bilinear", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (images - res[resize_lib]).sum() < 1e-5

def test_image_resize_batch_4():
    N = 7
    images = np.random.randn(N, 30, 30, 3)
    try:
        images = image_resize_batch(images, height=15, width=15, mode="black_bars",
                                    interpolation="bilinear", resize_lib="skimage")
        assert False
    except Exception:
        pass

def test_image_resize_batch_5():
    N = 7
    images = np.random.randint(0, 255, size=(N, 30, 30, 3), dtype=np.uint8)

    res = {}
    for resize_lib in ["skimage", "opencv", "PIL"]:
        try:
            res[resize_lib] = image_resize_batch(images, height=30, width=None, interpolation="nearest",
                                                 mode="black_bars", resize_lib=resize_lib)
        except Exception:
            pass

    for resize_lib in ["skimage", "opencv", "PIL"]:
        res[resize_lib] = image_resize_batch(images, height=30, width=30, interpolation="nearest",
                                             mode="black_bars", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (images - res[resize_lib]).sum() < 1e-5

def test_image_resize_batch_6():
    N = 7
    images = np.random.randint(0, 255, size=(N, 30, 30, 3), dtype=np.uint8)

    res = {}
    images = image_resize_batch(images, height=13, width=17, mode="black_bars",
                                interpolation="bilinear", resize_lib="skimage")
    for resize_lib in ["skimage", "opencv", "PIL"]:
        res[resize_lib] = image_resize_batch(images, height=13, width=17, mode="black_bars",
                                             interpolation="bilinear", resize_lib=resize_lib)

    for resize_lib in ["skimage", "opencv", "PIL"]:
        assert (images - res[resize_lib]).sum() < 1e-5
