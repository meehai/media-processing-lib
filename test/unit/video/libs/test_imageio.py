import numpy as np
from media_processing_lib.video.backends.imageio import MPLImageIOBackend
from media_processing_lib.utils import get_library_root

resources_dir = get_library_root() / "resources"
video_path = resources_dir / "test_video.mp4"
images_path = resources_dir / "images_path"


def test_imageio_constructor_1():
    try:
        MPLImageIOBackend("hello.wav")
    except:
        pass


def test_imageio_constructor_2():
    _ = MPLImageIOBackend(video_path)


def test_imageio_len_1():
    video = MPLImageIOBackend(video_path)
    assert len(video) == 253


def test_imageio_len_2():
    video = MPLImageIOBackend(video_path, n_frames=100)
    assert len(video) == 100


def test_imageio_len_4():
    video = MPLImageIOBackend(video_path, n_frames=100)
    try:
        _ = video[1000]
    except:
        pass
    assert len(video) == 100


def test_imageio_len_4():
    # We know our test video has 253 frames
    video = MPLImageIOBackend(video_path)
    try:
        _ = video[1000]
    except:
        pass
    assert len(video) == 253


def test_getitem_1():
    video = MPLImageIOBackend(video_path)
    frame = video[10]
    assert frame.shape == (480, 640, 3)


def test_getitem_2():
    video = MPLImageIOBackend(video_path)
    frame = video[20]
    assert frame.shape == (480, 640, 3)


def test_getitem_3():
    video = MPLImageIOBackend(video_path)
    frame = video[10]
    frame2 = video[10]
    assert np.allclose(frame, frame2)


def test_getitem_4():
    # We know our test video has 253 frames
    video = MPLImageIOBackend(video_path)
    frame = video[252]
    assert frame.shape == (480, 640, 3)

    try:
        _ = video[253]
    except:
        pass


if __name__ == "__main__":
    test_getitem_1()
