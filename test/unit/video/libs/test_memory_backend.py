import numpy as np
from media_processing_lib.video.backends.memory_backend import MemoryBackend
from media_processing_lib.utils import get_library_root

resources_dir = get_library_root() / "resources"
video_path = resources_dir / "test_video.mp4"
images_path = resources_dir / "images_path"

raw_data = np.random.randn(20, 30, 40, 3)


def test_memory_backend_constructor_1():
    MemoryBackend(raw_data)


def test_memory_backend_constructor_2():
    try:
        MemoryBackend(video_path)
    except:
        pass


def test_memory_backend_len_1():
    # we know our test dir has 100 images
    v = MemoryBackend(raw_data)
    assert len(v) == 20


def test_memory_backend_len_2():
    # we know our test dir has 100 images
    v = MemoryBackend(raw_data[10:15])
    assert len(v) == 5


def test_memory_backend_getitem_1():
    # we know our test dir has 100 images
    v = MemoryBackend(raw_data)
    item = v[10]
    assert item.shape == (30, 40, 3)


def test_memory_backend_getitem_2():
    # we know our test dir has 100 images
    v = MemoryBackend(raw_data)
    try:
        _ = v[100]
    except:
        pass
